#!/bin/sh
# name: ovh-debian-post-install
# descripton: complements default OVH debian install with very minimal requirements.
#

cat "$0" >/tmp/post-install-script.sh
echo -e "\n# Post-install-script original name: $0" >>/tmp/post-install-script.out
chmod 0600 /tmp/post-install-script.out /tmp/post-install-script.sh

( \
	echo "$0: beginning"
	echo "Current packages:" 
	dpkg -l
	apt-get -y install python || exit $?
	echo "New packages:" 
	dpkg -l
	echo "$0: end"
) 2>&1 | tee -a /tmp/post-install-script.out

RC=$?

test $RC -eq 0 && echo OK && exit 0

echo ERROR

exit $RC
